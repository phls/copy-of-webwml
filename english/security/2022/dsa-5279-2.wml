<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The wordpress package released in DSA-5279-1 had incorrect
dependencies that could not be satisfied in Debian stable: this update
corrects the problem. For reference, the original advisory text is
provided here again:</p>

  <p>Several vulnerabilities were discovered in Wordpress, a web blogging
  tool. They allowed remote attackers to perform SQL injection, create
  open redirects, bypass authorization access, or perform Cross-Site
  Request Forgery (CSRF) or Cross-Site Scripting (XSS) attacks.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 5.7.8+dfsg1-0+deb11u2.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wordpress">\
https://security-tracker.debian.org/tracker/wordpress</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5279-2.data"
# $Id: $
