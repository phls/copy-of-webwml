<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libgc is vulnerable to integer overflows in multiple places. In some cases,
when asked to allocate a huge quantity of memory, instead of failing the
request, it will return a pointer to a small amount of memory possibly
tricking the application into a buffer overwrite.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:7.1-9.1+deb7u1.</p>

<p>We recommend that you upgrade your libgc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-721.data"
# $Id: $
