<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Agostino Sarubbo of Gentoo discovered a heap buffer overflow write in the
rzip program (a compression program for large files) when uncompressing
maliciously crafted files.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1-2+deb8u1.</p>

<p>We recommend that you upgrade your rzip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2189.data"
# $Id: $
