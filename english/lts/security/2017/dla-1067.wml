<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Augeas is vulnerable to heap-based buffer overflow due to improper handling of
escaped strings. Attacker could send crafted strings that would cause the
application using augeas to copy past the end of a buffer, leading to a crash
or possible code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.0-1+deb7u1.</p>

<p>We recommend that you upgrade your augeas packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1067.data"
# $Id: $
