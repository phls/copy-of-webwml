<define-tag pagetitle>Debian 12 <q>bookworm</q> released</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>After 1 year, 9 months, and 28 days of development, the Debian 
project is proud to present its new stable version 12 (code name <q>bookworm</q>).</p>

<p><q>bookworm</q> will be supported for the next 5 years thanks to the 
combined work of the <a href="https://security-team.debian.org/">Debian Security team</a> 
and the <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a> team.</p>

<p>Following the <a href="$(HOME)/vote/2022/vote_003">2022 General Resolution about non-free firmware</a>, 
we have introduced a new archive area making it possible to separate non-free 
firmware from the other non-free packages:
<ul>
<li>non-free-firmware</li>
</ul>

Most non-free firmware packages have been moved from  <b>non-free</b> to 
<b>non-free-firmware</b>. This separation makes it possible to build a variety 
of official installation images.
</p>

<p>Debian 12 <q>bookworm</q> ships with several desktop environments, such as:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>This release contains over <b>11,089</b> new packages for a total count of <b>64,419</b> 
packages, while over <b>6,296</b> packages have been removed as <q>obsolete</q>. <b>43,254</b>
packages were updated in this release.

The overall disk usage for <q>bookworm</q> is <b>365,016,420 kB (365 GB)</b>, and is made up 
of <b>1,341,564,204</b> lines of code.</p> 

<p><q>bookworm</q> has more translated man pages than ever thanks to our 
translators who have made <b>man</b>-pages available in multiple languages 
such as: Czech, Danish, Greek, Finnish, Indonesian, Macedonian, 
Norwegian (Bokmål), Russian, Serbian, Swedish, Ukrainian, and Vietnamese. 
All of the <b>systemd</b> man pages are now completely available in German.</p>

<p>The Debian Med Blend introduces a new package: <b>shiny-server</b> which 
simplifies scientific web applications using  <b>R</b>.  We have kept to our 
efforts of providing Continuous Integration support for Debian Med team 
packages. Install the metapackages at version 3.8.x for Debian bookworm.</p>

<p>The Debian Astro Blend continues to provide a one-stop solution for 
professional astronomers, enthusiasts, and hobbyists with updates to almost 
all versions of the software packages in the blend. <b>astap</b> and 
<b>planetary-system-stacker</b> help with image stacking and astrometry 
resolution. <b> openvlbi</b>, the open source correlator, is now 
included.</p>

<p>Support for Secure Boot on ARM64 has been reintroduced: users of UEFI-capable 
ARM64 hardware can boot with Secure Boot mode enabled to take full advantage of 
the security feature.</p>

<p>Debian 12 <q>bookworm</q> includes numerous updated software packages 
(over 67% of all packages from the previous release), such as:

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (default email server) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>The GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux kernel 6.1 series</li>
<li>LLVM/Clang toolchain 13.0.1, 14.0 (default), and 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>
</p>

<p>
With this broad selection of packages and its traditional wide 
architecture support, Debian once again stays true to its goal of being 
<q>The Universal Operating System</q>. It is suitable for many different 
use cases: from desktop systems to netbooks; from development servers to 
cluster systems; and for database, web, and storage servers. At the same 
time, additional quality assurance efforts like automatic installation and 
upgrade tests for all packages in Debian's archive ensure that <q>bookworm</q> 
fulfills the high expectations that users have of a stable Debian release.
</p>

<p>
A total of nine architectures are officially supported for <q>bookworm</q>:
<ul>
<li>32-bit PC (i386) and 64-bit PC (amd64),</li>
<li>64-bit ARM (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>little-endian MIPS (mipsel),</li>
<li>64-bit little-endian MIPS (mips64el),</li>
<li>64-bit little-endian PowerPC (ppc64el),</li>
<li>IBM System z (s390x)</li> 
</ul>

32-bit PC (i386) no longer covers any i586 processor; the new minimum processor 
requirement is i686. <i>If your machine is not compatible with this requirement, 
it is recommended that you stay with bullseye for the remainder of its support 
cycle.</i>
</p>

<p>The Debian Cloud team publishes <q>bookworm</q> for several cloud computing 
services: 
<ul>
<li>Amazon EC2 (amd64 and arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (generic) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
The genericcloud image should be able to run in any virtualised environment, 
and there is also a nocloud image which is useful for testing the build process.
</p>

<p>GRUB packages will by default <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">no
longer run os-prober for other operating systems.</a></p>

<p>Between releases, the Technical Committee resolved that Debian <q>bookworm</q> 
should <a href="https://wiki.debian.org/UsrMerge">support only the merged-usr root filesystem layout</a>, dropping support 
for the non-merged-usr layout. For systems installed as buster or bullseye 
there will be no changes to the filesystem; however, systems using the older
layout will be converted during the upgrade.</p>


<h3>Want to give it a try?</h3>
<p>
If you simply want to try Debian 12 <q>bookworm</q> without installing it,
you can use one of the available <a href="$(HOME)/CD/live/">live images</a> which load and run the
complete operating system in a read-only state via your computer's memory.
</p>

<p>
These live images are provided for the <code>amd64</code> and
<code>i386</code> architectures and are available for DVDs, USB sticks,
and netboot setups. The user can choose among different desktop
environments to try: GNOME, KDE Plasma, LXDE, LXQt, MATE, and Xfce.
Debian Live <q>bookworm</q> has a standard live image, so it is also possible
to try a base Debian system without any of the graphical user interfaces.
</p>

<p>
Should you enjoy the operating system you have the option of installing
from the live image onto your computer's hard disk. The live image
includes the Calamares independent installer as well as the standard Debian Installer.
More information is available in the
<a href="$(HOME)/releases/bookworm/releasenotes">release notes</a> and the
<a href="$(HOME)/CD/live/">live install images</a> sections of
the Debian website.
</p>

<p>
To install Debian 12 <q>bookworm</q> directly onto your computer's storage 
device you can choose from a variety of installation media types to <a href="https://www.debian.org/download">Download</a> 
such as: Blu-ray Disc, DVD, CD, USB stick, or via a network connection.

See the <a href="$(HOME)/releases/bookworm/installmanual">Installation Guide</a> for more details.
</p>

# Translators: some text taken from: 

<p>
Debian can now be installed in 78 languages, with most of them available
in both text-based and graphical user interfaces.
</p>

<p>
The installation images may be downloaded right now via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (the recommended method),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, or
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; see
<a href="$(HOME)/CD/">Debian on CDs</a> for further information. <q>bookworm</q> will
soon be available on physical DVD, CD-ROM, and Blu-ray Discs from
numerous <a href="$(HOME)/CD/vendors">vendors</a> too.
</p>


<h3>Upgrading Debian</h3>
<p>
Upgrades to Debian 12 <q>bookworm</q> from the previous release, Debian 11
<q>bullseye</q>, are automatically handled by the APT package 
management tool for most configurations.
</p>

<p>Before upgrading your system, it is strongly recommended that you make a 
full backup, or at least back up any data or configuration information you 
can't afford to lose. The upgrade tools and process are quite reliable, but 
a hardware failure in the middle of an upgrade could result in a severely 
damaged system.

The main things you'll want to back up are the contents of /etc, 
/var/lib/dpkg, /var/lib/apt/extended_states and the output of:

<code>$ dpkg --get-selections '*' # (the quotes are important)</code>
      
<p>We welcome any information from users related to the upgrade from <q>bullseye</q> 
to <q>bookworm</q>. Please share information by filing a bug in the 
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">Debian
bug tracking system</a> using the <b>upgrade-reports</b> package with your results.
</p>

<p>
There has been a lot of development to the Debian Installer resulting in 
improved hardware support and other features such as fixes to graphical 
support on UTM, fixes to the GRUB font loader, removing the long wait at
the end of the installation process, and fixes to the detection of BIOS-bootable
systems. This version of the Debian Installer may enable non-free-firmware
where needed.</p>



<p>
The <b>ntp</b> package has been replaced with the <b>ntpsec</b> package,
with the default system clock service now being <b>systemd-timesyncd</b>; there
is also support for <b>chrony</b> and <b>openntpd</b>.
</p>


<p>As <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split"> <b>non-free</b> 
firmware has been moved to its own component in the archive</a>, if you have non-free
firmware installed it is recommended to add <b>non-free-firmware</b> to your 
APT sources-list.</p>

<p>It is advisable to remove bullseye-backports entries from APT source-list 
files before the upgrade; after the upgrade consider adding <b>bookworm-backports</b>.

<p>
For <q>bookworm</q>, the security suite is named <b>bookworm-security</b>; users 
should adapt their APT source-list files accordingly when upgrading.

If your APT configuration also involves pinning or <code>APT::Default-Release</code>,  
it is likely to require adjustments to allow the upgrade of packages to the new 
stable release. Please consider <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">disabling APT pinning</a>.
</p>

<p>The OpenLDAP 2.5 upgrade includes some <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#openldap-2.5">incompatible changes
which may require manual intervention</a>. Depending on configuration the
<b>slapd</b> service may remain stopped after the upgrade until new 
configuration updates are completed.</p>


<p>The new <b>systemd-resolved</b> package will not be installed automatically 
on upgrades as it <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved">has been split
into a separate package</a>. If using the systemd-resolved system service, please install the new package manually after
the upgrade, and note that until it has been installed, DNS resolution may no 
longer work as the service will not be present on the system.</p>



<p>There are some <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#changes-to-system-logging">changes to system logging</a>;
the <b>rsyslog</b> package is no longer needed on most systems, and is not installed
by default. Users may change to <b>journalctl</b> or use the new
<q>high precision timestamps</q> that <b>rsyslog</b> now uses.
</p>


<p><a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble">Possible issues during the upgrade</a> include
Conflicts or Pre-Depends loops which can be solved by removing and eliminating
some packages or forcing the re-installation of other packages. Additional 
concerns are <q>Could not perform immediate configuration ...</q> errors for which
one will need to keep <b>both</b> <q>bullseye</q> (that was just removed) and 
<q>bookworm</q> (that was just added) in the APT source-list file, and 
File Conflicts which may require one to forcibly remove packages.
As mentioned, backing the system up is the key to a smooth upgrade should any 
untoward errors occur.</p> 


<p>There are some packages where Debian cannot promise to provide minimal 
backports for security issues. Please see the <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">Limitations
in security support</a>.</p>


<p>
As always, Debian systems may be upgraded painlessly, in place,
without any forced downtime, but it is strongly recommended to read
the <a href="$(HOME)/releases/bookworm/releasenotes">release notes</a> as
well as the <a href="$(HOME)/releases/bookworm/installmanual">installation
guide</a> for possible issues, and for detailed instructions on
installing and upgrading. The release notes will be further improved and
translated to additional languages in the weeks after the release.
</p>


<h2>About Debian</h2>

<p>
Debian is a free operating system, developed by
thousands of volunteers from all over the world who collaborate via the
Internet. The Debian project's key strengths are its volunteer base, its
dedication to the Debian Social Contract and Free Software, and its
commitment to provide the best operating system possible. This new
release is another important step in that direction.
</p>


<h2>Contact Information</h2>

<p>
For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.
</p>

