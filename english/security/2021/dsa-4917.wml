<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30506">CVE-2021-30506</a>

    <p>@retsew0x01 discovered an error in the Web App installation interface.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30507">CVE-2021-30507</a>

    <p>Alison Huffman discovered an error in the Offline mode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30508">CVE-2021-30508</a>

    <p>Leecraso and Guang Gong discovered a buffer overflow issue in the Media
    Feeds implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30509">CVE-2021-30509</a>

    <p>David Erceg discovered an out-of-bounds write issue in the Tab Strip
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30510">CVE-2021-30510</a>

    <p>Weipeng Jiang discovered a race condition in the aura window manager.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30511">CVE-2021-30511</a>

    <p>David Erceg discovered an out-of-bounds read issue in the Tab Strip
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30512">CVE-2021-30512</a>

    <p>ZhanJia Song discovered a use-after-free issue in the notifications
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30513">CVE-2021-30513</a>

    <p>Man Yue Mo discovered an incorrect type in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30514">CVE-2021-30514</a>

    <p>koocola and Wang discovered a use-after-free issue in the Autofill
    feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30515">CVE-2021-30515</a>

    <p>Rong Jian and Guang Gong discovered a use-after-free issue in the file
    system access API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30516">CVE-2021-30516</a>

    <p>ZhanJia Song discovered a buffer overflow issue in the browsing history.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30517">CVE-2021-30517</a>

    <p>Jun Kokatsu discovered a buffer overflow issue in the reader mode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30518">CVE-2021-30518</a>

    <p>laural discovered use of an incorrect type in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30519">CVE-2021-30519</a>

    <p>asnine discovered a use-after-free issue in the Payments feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30520">CVE-2021-30520</a>

    <p>Khalil Zhani discovered a use-after-free issue in the Tab Strip
    implementation.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 90.0.4430.212-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4917.data"
# $Id: $
