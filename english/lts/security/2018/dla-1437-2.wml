<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update for slurm-llnl introduced a regression in the fix for
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10995">CVE-2018-10995</a> which broke accounting.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
14.03.9-5+deb8u4.</p>

<p>We recommend that you upgrade your slurm-llnl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1437-2.data"
# $Id: $
