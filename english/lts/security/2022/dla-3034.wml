<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Nathan Davison discovered that HAProxy, a load balancing reverse proxy, did not
correctly reject requests or responses featuring a transfer-encoding header
missing the <q>chunked</q> value which could facilitate a HTTP request smuggling
attack. Furthermore several flaws were discovered in DNS related functions that
may trigger infinite recursion leading to a denial of service or to information
leakage due to a out-of-bounds read.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.7.5-2+deb9u1.</p>

<p>We recommend that you upgrade your haproxy packages.</p>

<p>For the detailed security status of haproxy please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/haproxy">https://security-tracker.debian.org/tracker/haproxy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3034.data"
# $Id: $
