<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two heap buffer over read conditions were found in gpac.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13005">CVE-2018-13005</a>

    <p>Due to an error in a while loop condition, the function urn_Read in
    isomedia/box_code_base.c has a heap-based buffer over-read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13006">CVE-2018-13006</a>

    <p>Due to an error in a strlen call, there is a heap-based buffer over-read
    in the isomedia/box_dump.c function hdlr_dump.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.5.0+svn5324~dfsg1-1+deb8u1.</p>

<p>We recommend that you upgrade your gpac packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1432.data"
# $Id: $
