<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An integer overflow (with a resultant heap-based buffer overflow) was
discovered in the nbd Network Block Device server. A value of 0xffffffff in the
name length field could have caused a zero-sized buffer to be allocated for the
name, resulting in a write to a dangling pointer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26495">CVE-2022-26495</a>

    <p>In nbd-server in nbd before 3.24, there is an integer overflow with a
    resultant heap-based buffer overflow. A value of 0xffffffff in the name
    length field will cause a zero-sized buffer to be allocated for the name,
    resulting in a write to a dangling pointer. This issue exists for the
    NBD_OPT_INFO, NBD_OPT_GO, and NBD_OPT_EXPORT_NAME messages.</p</li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:3.15.2-3+deb9u1.</p>

<p>We recommend that you upgrade your nbd packages.</p>

<p>Thanks to Wouter Verhelst for help in preparing this update.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2944.data"
# $Id: $
