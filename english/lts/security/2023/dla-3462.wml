<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been addressed in Wordpress, a
popular content management framework.</p>

<p>WordPress Core is vulnerable to Directory Traversal via the �wp_lang�
parameter. This allows unauthenticated attackers to access and load arbitrary
translation files. In cases where an attacker is able to upload a crafted
translation file onto the site, such as via an upload form, this could be also
used to perform a Cross-Site Scripting attack.</p>

<p>For Debian 10 buster, this problem has been fixed in version
5.0.19+dfsg1-0+deb10u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3462.data"
# $Id: $
