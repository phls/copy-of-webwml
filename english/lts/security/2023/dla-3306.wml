<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential Denial of Service (DoS)
vulnerability in Django, a popular Python-based web development framework.</p>

<p>Parsed values of the <code>Accept-Language</code> HTTP headers are cached by
Django order to avoid repetitive parsing. This could have led to a potential
denial-of-service attack via excessive memory usage if the raw value of
<code>Accept-Language</code> headers was very large.</p>

<p><code>Accept-Language</code> headers are now limited to a maximum length
specifically in order to avoid this issue.</p>


<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23969">CVE-2023-23969</a></li>
</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:1.11.29-1+deb10u6.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3306.data"
# $Id: $
