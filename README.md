# Copy of webwml

I'm making a copy of the folders below (english and portuguse translations files)
because there is an idea to delete them from the original webwml repository.

- consultants
- devel
- lts
- News
- partners
- security
- users
- vote

Make a script on the future

```
rsync -Hpagouvt /srv/traducao/webwml/english/consultants/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/devel/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/lts/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/News/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/partners/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/security/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/users/ /srv/traducao/copy-of-webwml/english/
rsync -Hpagouvt /srv/traducao/webwml/english/vote/ /srv/traducao/copy-of-webwml/english/

rsync -Hpagouvt /srv/traducao/webwml/portuguese/consultants/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/devel/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/lts/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/News/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/partners/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/security/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/users/ /srv/traducao/copy-of-webwml/portuguese/
rsync -Hpagouvt /srv/traducao/webwml/portuguese/vote/ /srv/traducao/copy-of-webwml/portuguese/

```