<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Rails HTML Sanitizers, an
HTML sanitization library for Ruby on Rails applications. An attacker
could launch cross-site scripting (XSS) and denial-of-service (DoS)
attacks through crafted HTML/XML documents.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23517">CVE-2022-23517</a>

    <p>Certain configurations use an inefficient regular expression that
    is susceptible to excessive backtracking when attempting to
    sanitize certain SVG attributes. This may lead to a denial of
    service through CPU resource consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23518">CVE-2022-23518</a>

    <p>Cross-site scripting via data URIs when used in combination with
    Loofah >= 2.1.0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23519">CVE-2022-23519</a>

    <p>XSS vulnerability with certain configurations of
    Rails::Html::Sanitizer may allow an attacker to inject content if
    the application developer has overridden the sanitizer's allowed
    tags in either of the following ways: allow both <q>math</q> and
    <q>style</q> elements, or allow both <q>svg</q> and <q>style</q> elements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23520">CVE-2022-23520</a>

    <p>XSS vulnerability with certain configurations of
    Rails::Html::Sanitizer due to an incomplete fix of
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-32209">CVE-2022-32209</a>. Rails::Html::Sanitizer may allow an attacker to
    inject content if the application developer has overridden the
    sanitizer's allowed tags to allow both <q>select</q> and <q>style</q>
    elements.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.0.4-1+deb10u2.</p>

<p>We recommend that you upgrade your ruby-rails-html-sanitizer packages.</p>

<p>For the detailed security status of ruby-rails-html-sanitizer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer">https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3566.data"
# $Id: $
