#use wml::debian::template title="Platform for Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>DPL Platform</SMALL><BR>
    <SMALL>2019-03-17</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>

<H2 CLASS="section">Executive summary</H2>
<P>Hi, my name is <A HREF="https://wiki.debian.org/highvoltage">Jonathan Carter</A>, also known as <EM>highvoltage</EM> with the Debian account name of <EM>jcc</EM>, and I'm running for DPL. </P><DIV CLASS="summary">
<B>Broad goals as DPL</B>:
<UL CLASS="itemize">
<LI CLASS="li-itemize"> My intention is not to persue big, hairy and audacious goals. Instead, I aim to improve communication to help enable our community to fix existing problems and help make ideas flourish.</LI>
<LI CLASS="li-itemize"> I want to promote polish and help drive better quality all over Debian, whether that's in our final products or community processes.</LI>
<LI CLASS="li-itemize"> I want Debian to be an attractive project to participate in. Every Debian developer should be outright proud to be associated with the project.</LI>
<LI CLASS="li-itemize"> The above might come across as playing it safe, and not being very ambitious, but I think big things can be achieved with baby steps and I hope to convey that in my platform.</LI>
</UL>
</DIV>

<H2 CLASS="section">1. Introduction</H2>

<H3 CLASS="subsection">1.1. Who I am</H3>

<div style="min-height: 200px;">
<img style="float: left; padding-right: 10px;" src="jcc.jpg" />
<P>I am 37 years old and live in Cape Town, South Africa, where I'm also from.</P>
<P>I work part time for an educational institute where I do sysadmin work and work on a Debian derivative that the institutional network use in their centres across Africa. The rest of the time I do mostly Debian related contract work.</P>
<P> I'm a long-time free software advocate who has worked in commercial, non-profit, public sector and educational environments over the last 15 years.
</div>

<H3 CLASS="subsection">1.2 Brief history with Debian</H3>

<P>I started using Linux in 1999, and in 2003, I was excited about it and wanted to teach kids about it in schools. I learned that a local non-profit was looking into similar initiatives and I started talking to them, volunteering, doing some contract work and then eventually working there full-time. My colleague at the time, Thomas Black re-introduced me to Debian. I was previously unimpressed with Debian because its installer back then, boot-floppies, almost never worked for me and when it did everything was ancient. He introduced me to Debian unstable and I got hooked, he told me that I should learn packaging and apply for DD status and become 'highvoltage@debian.org'. When he suggested that it felt like a completely alien concept to me, I had never even thought of considering being part of something as cool as a Linux distribution project before, but the idea got firmly planted into my head.</P>

<P>Later that year the first Ubuntu version was released, and I took a detour from Debian into the Ubuntuverse for a few years.</P>

<P>During DebConf7, I discovered the live video streams and got hooked. I also read the new maintainer's guide for the first time around then. In 2012 I made it to my first DebConf in Nicaragua which was a great experience, and in 2016 we successfully hosted a DebConf in Cape Town. In 2017 I became part of the newly formed DebConf Committee. I've made various different contributions to DebConf over the years, even <a href="https://jonathancarter.org/files/debian/debconf/pollito-adventure.pdf">graphic</a> <a href="https://jonathancarter.org/files/debian/debconf/pollito-dc16-guide.pdf">stories</a> featuring the DebConf chicken.</P>

<P>I became a Debian Maintainer in 2016, and a Debian Developer in 2017. That makes me a relatively young DD in terms of how long I've been a member. I now actively maintain over 60 packages and have recently joined the debian-live project to help improve the quality of our live images.</P>

<P>I try to summarise my Debian activities on my <a href="https://wiki.debian.org/highvoltage">wiki page</a>, although it's not always easy keeping it up to date. That page also links to my monthly free software activity recaps.</P>

<H3 CLASS="subsection"> 1.3. Confidence in Debian</H3>

<P>I like the concept of the "Universal Operating System". To me it means that Debian is adaptable to different technologies, situations and use cases. I'm constantly amazed at the great work that Debian Developers and all the Debian contributors do in all shapes and forms on a daily basis.</P>

<P>When Debian was founded, the average computer user was busy migrating from MS-DOS to Windows 3.1 and learning how to use a mouse. Windows 95 hasn't even been released yet. Since then, so much has changed. The average internet user now know what a VPN is. We have common awareness of the dangers of mass surveillance, and the technology that we originally envisioned as providing answers to all the world's problems are now often used against our species as a whole.</P>

<P>Immensely positive changes are also happening. The <a href="https://riscv.org/">RISC-V foundation</a> has been formed, making great strides in developing a high-end free hardware central processing unit. Their work has also lead to <a href="https://wavecomp.ai/mips-open/">MIPS following suite</a> who have announced that they will release open versions of the MIPS instruction set. At the same time, Purism is also working towards releasing a <a href="https://puri.sm/products/librem-5/">fully free phone.</a></P>

<P>With all the good and bad things on our radar, Debian is more relevant than ever. The world needs a fully free system with stable releases and security updates that puts its users first, that's commercially friendly but at the same time doesn't have any hidden corporate agendas. Debian is unique and beautiful and important, and we shouldn't allow that message to get lost in the noise that exists out there.</P>

<P>I want us to focus on our shared passion and vision for Debian instead of the very few places where we differ. Too often, the small things are blown out of proportion by a small minority or by the media. We can't let that drown out the vast positivity and goodness in our project that keeps our contributors churning out good work on a daily basis.</P>

<H2 CLASS="section">2.  My DPL mission statement</H2>

<H3> 2.1. High level goals </H3>

<OL>
<LI><B>I do not intend to solve every problem in Debian during this term:</B> I believe it's more beneficial to pull together as a community and work together to focus on our most pressing core issues and iterate over them.</LI>

<LI><B>I don't want my role as DPL to be purely administrative:</B> I think it's critical that there's someone in the project who has a stoic view of the Debian project system-wide, since we often tend to get wrapped up in our own problems as individuals inside the project.</LI>

<LI><B>DPL as enabler:</B> Some of my ideas can be implemented without being a DPL, but being DPL comes with some spotlight and attention and makes it a lot easier to drive certain ideas forward.</LI>

<LI><B>Make community processes a first class citizen in Debian:</B> As much as our technical processes are.</LI>
<LI><B>Make the DPL more approachable:</B> Previous DPLs have been great at communicating their work to the project, I aim to make that more of a two-way street.</LI>
<LI><B>Promote new ideas and experimentation in the project:</B> Track the most requested ideas in Debian and advertise these for teams who might be willing to take them up.</LI>
<LI><B>Foster our community:</B> It's important to gain more contributors, but I think it's equally important to make contributing to Debian enjoyable for existing developers too. When things aren't going well, it feels futile recruiting new developers. If we make Debian a fantastic project to work in for all our existing developers, we will naturally attract more contributors.</LI>
<LI><B>Small details matter:</B> We've become too accustomed to small things that don't work. If you're used to them, you're also used to working around them. Through the eyes of a newcomers though, the Debian experience can often feel like a case of death by a thousand papercuts.</LI>
</OL>


<h3> 2.2. Execution </H3>

<P>Overall, I want to help Debian be better at talking about the actual issues that affect us more often, without having another daunting thread like some famous ones that had before. I want to go as far as to say that I care more about fixing communication and our internal problem solving mechanisms than the actual problems themselves. I hope to promote ways of working that will be beneficial to Debian long after I'm not DPL any more.</P>

<P>Below are some of the ideas I am considering to implement in order to achieve the above. They are not stuck in stone, and I will consult with the community at large when implementing them. If they are deemed largely unsuitable, I will not mind backing off on any of these and we can work on alternative methods for solving those problems.</P>

<OL>
<LI><B>Promote and use #debian-meeting as a meeting room:</B> Many teams have IRC meetings in their own channel. It would bring a lot of visibility if Debian teams would instead use a slot in the #debian-meeting IRC channel. Not only would it make it easier for interested parties to keep up with what's going on in the scroll-back, but it might also help attract new members to your team as they become familiar with its problems.</LI>
<LI><B>Weekly community meetings:</B> Have a project-wide community meeting on a weekly basis with the DPL and helpers (in whatever form they may exist) present. This would alternate in time to make it easier for people from various time-zones. The idea is to shine a light on the most pressing issues that concern the community, and work towards solutions. I'm sure some people dread the idea because of trolls, but we can moderate and/or limit participants if needed.</LI>

<LI><B>Implement a 100 papercuts campaign:</B> Create a project to identify the 100 most annoying small problems with Debian. These would typically be items that can be solved with a day's worth of work. This could be a technical bug or a problem in the Debian project itself. The aim would be to fix these 100 bugs by the time Buster (Debian 10) is released. A new cycle can also be started as part of an iterative process that's based on time and/or remaining bug count.</LI>
<LI><B>Hardware enablement project:</B> Between all the new architectures listed above, and new devices like free phones, hardware enablement becomes really important. I believe we should have budget available for getting hardware to developers who care about enabling Debian on those devices. I believe the DPL could also spend some time with hardware vendors to get some free (or loan) samples, as well as preferential pricing for all Debian Developers.</LI>
<LI><B>Debian Women Meetups: </B>As DPL, I wish to create a project where women in any city of the world can organise monthly meetings and Debian will cover the cost of the refreshments (similar to how it's done for bug squashing parties). We often talk about how serious we are about getting more women involved in the project, but if we're serious we have to be willing to put our money where our mouths are. These meetings could be as simple as just talking about Debian and demo'ing it. As soon as someone is interested and start using Debian, they immediately learn skills that they can transfer to someone else. We have a huge base of potential contributors that we're not targetting enough.</LI>
<LI><B>Better financial understanding:</B> In the past, people have asked for better understanding of how Debian spends funds, and how much is available. The way that Debian's accounting works across trusted organisations can make real-time exact numbers really difficult, but I think that basic reports on a regular basis that list all recent spending (sprints, DebConf, hardware, etc) along with recent balances will be enough to give a sufficient overview of how Debian is managing its funds.</LI>
<LI><B>DPL pseudo-bugs:</B> As DPL, I will also endeavour to take some time off. I plan to not spend weekends working on DPL matters unless there is something really urgent (or exciting) that warrants attention. If any part of the DPL role becomes too demanding, I will consider it a bug and will file a public bug about it. I recommend a DPL pseudo-package (<a href="https://www.debian.org/Bugs/pseudo-packages">more on those here</a>) that can be used to file bugs against the DPL role, and also be used to make common requests to the DPL (similar to how you can file ITP/RFS/O/etc bugs in the wnpp pseudo-package).</LI>
<LI><B>Make it clear how processes work, and how to submit feedback:</B> I know it sounds redundant to say this, but every community process should be well documented in an obvious place, but in addition to that, there should also be a clear method to file bugs/objections/improvement to a process, just as we do with the Debian Policy Manual for packaging. Projects like DebConf could <I>really</I> benefit from this in my opinion.</LI>
</OL>

<P>None of the above is set in stone, and I will work with the community as I pursue these for feedback. Plans may certainly change, but I thing that working through the above may put the project on a good footing to strengthening our community and move on to larger ideas. I'll leave you with one of my favourite quotes:</P>

<DIV CLASS="quote">
<P>“Great minds discuss ideas. Average minds discuss events. Small minds discuss people.” <BR> <I> – Eleanor Roosevelt</I></P>
</DIV>

<H2> 3. Consequences to existing work </H2>

<p>If elected, at least to some extent, I plan on cutting back on existing roles in Debian to maximise the amount of energy available for the DPL role:</p>

<OL>
<LI><B>DebConf:</B> I'll step down from the DebConf Committee, which shouldn't at all be a big disruption since our biggest recurring task is the bid decision for the next DebConf, which is now concluded for this year. I plan to remain involved in other DebConf areas. I will also step down from bursaries after DC19, but will work on contingency for next year.
</LI>
<LI><B>Package maintenance:</B> I plan to release any ITP bugs that I have filed and won't take on any invasive new packaging work. I will however continue maintaining my existing packages.</LI>
<LI><B>Package sponsoring:</B> This is an activity I do when I have some spare time, I will continue doing so, but probably just less of it.</LI>
<LI><B>Goofing around:</B> DPLs are supposed to always be serious, right?</LI>
</OL>

<H2> 4. Some factors to weigh up. </H2>

<H3> 4.1. Reasons to consider me for DPL </H3>

<OL>
<LI><B>New energy:</B> With the above plans (or similar to those), I believe I can inject new energy into the Debian project. I think that Debian Developers deserve a project leader that can keep the positive energy flowing and make everyone proud to be part of the Debian project. </LI>

<LI><B>Sanity:</B> I'm not going to introduce any wild ideas to the project, instead, I aim to find ways to keep it grounded and create stable platforms that we can use to build on towards bigger and more ambitious ideas.</LI>

<LI><B>Timing:</B> This year happens to be a good time for me to be a DPL. My personal life is relatively uneventful right now, and I don't have a family to take care of and I have some flexibility work-wise without being overcommitted at all.</LI>
</OL>

<H3> 4.2. Weaknesses in my campaign </H3>

<OL>
<LI><B>Experience</B>: I don't have experience leading a project as large and complex (or even complicated) as Debian. I've also only been a Debian Developer for a relatively short period of time. However, I believe that being aware of this will at least prevent me from flying too close to the sun, my main goal for my term will be to reduce existing friction and mitigate or solve existing problems. </LI>
<LI><B>Public appearances:</B> I live in a relatively remote part of the world, and due to visa requirements and travel time, I won't be able to travel and talk about Debian as much as previous DPLs were able to. I might be able to mitigate this by staying in an area for a slightly longer time and do a series of talks at a time, I can often work remotely which help make this possible.
</LI>
<LI><B>Temperament:</B> I can be very emotional, and sometimes it bursts out a little. Over the years I've learned to channel my emotional energy into something more positive. I aim to practice a stoistic outlook  as DPL and spend some time reading up on diplomacy, if elected.</LI>
</OL>

<H2> 5. Acknowledgements </H2>

<UL>
<LI>I used <a href="https://www.debian.org/vote/2010/platforms/zack">Zack's platform layout</a> as a base for mine.</LI>
<LI>Thanks to Laura Arjona Reina and Martin Michlmayer who both fixed many typos and language problems in this platform.</LI>
</UL>

<H2> A. Rebuttals </H2>

<P>First off, I have met Joerg, Sam and Martin in real life, and it's an honour and a privilege to be running a DPL campaign alongside them.</P>

<P>While I like all 3 of them, I think it's important to be real and address some problems in their platforms.</P>

<P>At the time writing, Simon has not yet posted anything since his nomination, so at present I can't address his platform.</P>

<H3> A.1. Joerg Jaspert </H3>

<P><a href="https://www.debian.org/vote/2019/platforms/joerg">https://www.debian.org/vote/2019/platforms/joerg</a></P>

<P>Joerg has made lots of contributions over the years and still wears many hats, I'm grateful for all the work that he does.</P>

<P>He asks some important questions on his platform, but I've found that the details on how to address those questions were a bit thin. The role of DPL that he describes seem somewhat passive and reactive. In my humble opinion Debian needs a proactive DPL right now, who won't shy away from the hard issues, and will take them on head on within the community and appropriate teams.</P>

<P>He mentions on his platform that he is willing to work with anyone who wants to improve processes in Debian, which is great, but it is my opinion that where we are now, the DPL should also drive some processes forward without waiting on any kind of external pressure to do so.</P>

<H3> A.2. Sam Hartman </H3>

<P><a href="https://www.debian.org/vote/2019/platforms/hartmans">https://www.debian.org/vote/2019/platforms/hartmans</a></P>

<P>I like that Sam recognises mediation as an important aspect and also his feelings about the non-violent communication framework. Many other items he talks about resonate with me.</P>

<P>In his platform, he talks about the growing amount of free software that's available, but I couldn't find anything in his platform that directly addressed growing the community. The free software world out there is flourishing, and we should think of ways to make Debian scale up along with it. While it's not a technical role of the DPL, I do think that it's important for the DPL to have that in mind in all aspects when considering community health.</P>

<P>He mentions that as DPL he would have a number of tools available to drive decision making, but is very light on exactly what those should be, in addition, he mentions that GRs are under utilized. I think that GRs should remain a last resort and that there are better ways to gauge the community's stance on a topic when you need it. If a poll is needed, it's better to do a proper poll than to use a GR as a generic tool.</P>

<H3> A.3. Martin Michlmayr </H3>

<P><a href="https://www.debian.org/vote/2019/platforms/tbm">https://www.debian.org/vote/2019/platforms/tbm</a></P>

<P>I like that Martin also wants to actively facilitate discussion and generate buzz around Debian. Both are important in Debian right now which is also why I addressed those in my platform as well.</P>

<P>Having said that, I find his platform overall a bit depressing. I think he is in some ways a bit overcritical since he mentions some valid negative items, but in my experience I've also had many positive flip-side experiences that go along with those that aren't at all present in his platform. In my opinion he paints Debian in a more negative light than it deserves and I'm not sure if that's a good property of a DPL.</P>

<P>I think it's possible that because he hasn't been active in Debian in recent years, he may have missed out on a lot of the good things in Debian, and possibly a lot that has improved over the period since he's been more active.</P>

<P>Part of his sustainability plan is thinking about funding for people for their Debian work. While I think that it could be a healthy topic to explore, I also think that it's going to be a more controversial topic than he realises, and to be honest I think that's a topic that I would rather want to defer for this year. I think it's more important for Debian to focus on the top problems for the project right now, to heal and grow and get a good footing as a community. Martin also <a href="https://lists.debian.org/msgid-search/20190320173829.GB3022@jirafa.cyrius.com">thinks that it's time to ask questions like 'Why the DPL is not a paid position?'</a>, although personally, I don't want to spend the next year on that topic in all the various mailing lists when I believe that we could benefit from exploring some more immediate issues.</P>

<H2> B. Changelog </H2>

<P> This platform is version controlled in a <a href="https://salsa.debian.org/jcc/dpl-platform">git repository.</a> </P>

<ul>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.00">1.00</a>: Initial public release.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.01">1.01</a>: Improve summary on high-level goals.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.02">1.02</a>: Build a styled html, plain wml and plain text version of this page, fix photo, no text changes.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.03">1.03</a>: Merge <a href="https://salsa.debian.org/jcc/dpl-platform/merge_requests/1">MR#1</a> and <a href="https://salsa.debian.org/jcc/dpl-platform/merge_requests/2">MR#2</a>, fixing various mistakes. Fix minor style issue and changelog wording.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.04">1.04</a>: Add rebuttals, fix more typos.</LI>
</ul>

<BR>

</DIV> <!-- END MAIN -->


