<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3390">CVE-2023-3390</a>

    <p>A use-after-free flaw in the netfilter subsystem caused by incorrect
    error path handling may result in denial of service or privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3610">CVE-2023-3610</a>

    <p>A use-after-free flaw in the netfilter subsystem caused by incorrect
    refcount handling on the table and chain destroy path may result in
    denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20593">CVE-2023-20593</a>

    <p>Tavis Ormandy discovered that under specific microarchitectural
    circumstances, a vector register in AMD <q>Zen 2</q> CPUs may not be
    written to 0 correctly.  This flaw allows an attacker to leak
    sensitive information across concurrent processes, hyper threads
    and virtualized guests.</p>

    <p>For details please refer to
    <a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a> and
    <a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">
    https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a></p>

    <p>This issue can also be mitigated by a microcode update through the
    amd64-microcode package or a system firmware (BIOS/UEFI) update.
    However, the initial microcode release by AMD only provides
    updates for second generation EPYC CPUs.  Various Ryzen CPUs are
    also affected, but no updates are available yet.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 5.10.179-3.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5461.data"
# $Id: $
