<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in libarchive, a library for
reading and writing archives in various formats. An attacker can take
advantage of these flaws to cause a denial-of-service against an
application using the libarchive library (application crash), or
potentially execute arbitrary code with the privileges of the user
running the application.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.4-3+wheezy2.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-554.data"
# $Id: $
