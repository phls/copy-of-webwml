<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Thunderbird, which could
result in denial of service or the execution of arbitrary code.</p>

<p>Debian follows the Thunderbird upstream releases. Support for the
102.x series has ended, so starting with this update we're now
following the 115.x series.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 1:115.3.1-1~deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 1:115.3.1-1~deb12u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>For the detailed security status of thunderbird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/thunderbird">\
https://security-tracker.debian.org/tracker/thunderbird</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5513.data"
# $Id: $
