<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two issues in znc, a modular IRC
bouncer:</p>

<ul>

  <li>There was insufficient validation of lines coming from the network
    allowing a non-admin user to escalate his privilege and inject rogue
    values into znc.conf. (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14055">CVE-2018-14055</a>)</li>

  <li>A path traversal vulnerability (via "../" being embedded in a web skin
    name) to access files outside of the allowed directory.
    (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14056">CVE-2018-14056</a>)</li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these issues have been fixed in znc version
1.4-2+deb8u1.</p>

<p>We recommend that you upgrade your znc packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1427.data"
# $Id: $
