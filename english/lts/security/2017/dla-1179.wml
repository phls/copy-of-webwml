<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Rod Widdowson of Steading System Software LLP discovered a coding error
in the <q>Dynamic</q> metadata plugin of the Shibboleth Service Provider,
causing the plugin to fail configuring itself with the filters provided
and omitting whatever checks they are intended to perform.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.4.3+dfsg-5+deb7u2.</p>

<p>We recommend that you upgrade your shibboleth-sp2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1179.data"
# $Id: $
