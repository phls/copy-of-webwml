<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the wpewebkit
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30887">CVE-2021-30887</a>

    <p>Narendra Bhati discovered that processing maliciously crafted web
    content may lead to unexpectedly unenforced Content Security
    Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30890">CVE-2021-30890</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to universal cross site scripting.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.34.3-1~deb11u1.</p>

<p>We recommend that you upgrade your wpewebkit packages.</p>

<p>For the detailed security status of wpewebkit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpewebkit">\
https://security-tracker.debian.org/tracker/wpewebkit</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5031.data"
# $Id: $
