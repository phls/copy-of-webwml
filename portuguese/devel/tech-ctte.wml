#use wml::debian::template title="Comitê técnico do Debian" BARETITLE="true"
#use wml::debian::translation-check translation="11091d9cffbf50478efe09511281d86d60fd1b15" 
#use wml::debian::toc

<p>O comitê técnico é estabelecido pela seção 6 da
<a href="constitution">Constituição Debian</a>. Ele é o corpo que
toma a decisão final em disputas técnicas no projeto Debian.
</p>

<toc-display/>

<toc-add-entry name="referquestions">Como levar uma questão ao comitê técnico</toc-add-entry>

<ol>

<li>Antes de apelar para o comitê técnico sobre uma decisão, você deve
    tentar resolvê-la você mesmo(a). Entre em uma discussão construtiva e tente
    entender o ponto de vista da outra pessoa. Se, após a discussão, vocês
    tiverem identificado uma questão técnica sobre a qual não conseguem concordar,
    vocês podem submetê-la ao comitê:
</li>

<li>Escreva um resumo do desentendimento, preferencialmente com a concordância
    do seu oponente, e envie-o para o sistema de gerenciamento de bugs
    <em>como um novo bug </em>contra o pseudopacote <tt>tech-ctte</tt>.
    No seu resumo, mencione quaisquer números de bugs relevantes existentes e
    urls de arquivos de listas de discussão.
</li>

<li>Mande um e-mail para todas as partes relevantes envolvidas, convidando-as
    para se inscrever no bug. Se houver bug(s) existente(s) sobre a
    questão, coloque o novo bug tech-ctte para bloqueá-los (mas se você não
    souber como fazer isso, não se preocupe - nós faremos por você.)
</li>

<li>O comitê irá discutir sua questão no bug tech-ctte.
    Nós geralmente não colocaremos os(as) participantes em cópia individual na
    discussão, a não ser que nós os(as) convidemos para a conversa para
    perguntar algo de especifico. Todos(as) que estiverem interessados(as) na
    questão deverão se inscrever no bug usando o BTS.
</li>

<li>O comitê terá como objetivo tomar uma decisão o mais rapidamente possível.
    Na prática, este processo provavelmente levará várias semanas, ou talvez
    mais. Se a questão é particularmente urgente, por favor avise.
</li>

<li>Algumas vezes, um lado ou outro é convencido, durante as deliberações do
    comitê, pelo mérito dos argumentos do outro lado. Isto é uma coisa boa!
    Se isto acontecer, o comitê não precisa tomar uma decisão formal, e o
    relatório de bug pode ser fechado ou transferido, conforme for apropriado.
</li>

</ol>

<h3>Algumas notas sobre entrar em contato com o comitê</h3>

<ul>

<li>Um debate sadio e vigoroso é importante para certificar que todos os
    aspectos de uma questão foram completamente explorados. Quando
    discutir questões técnicas com outros(as) desenvolvedores(as), você deve
    estar pronto(a) para ser questionado(a)! Você também deve estar preparado(a)
    para ser convencido(a)! Não há vergonha em ver o mérito de bons argumentos.
</li>

<li>Por favor, conduza as suas discussões técnicas com outros(as)
    mantenedores(as) de forma calma e civilizada; não use insultos, nem
    questione suas competências. Em vez disso, direcione-se aos argumentos de
    seu oponente.
</li>

<li>O comitê tem poderes somente para tomar decisões técnicas. Se você
    acha que alguém está se comportando de modo inadequado, o comitê
    provavelmente não pode ajudá-lo(a) muito. Você pode querer falar com
    o líder do projeto, <tt>leader@debian.org</tt>.
</li>

<li>O tráfego do bug também aparecerá na lista de discussão do comitê,
    <a href="https://lists.debian.org/debian-ctte/">debian-ctte@lists.debian.org</a>.
    Qualquer outra pessoa que deseja fazê-lo pode se inscrever na lista de
    discussão debian-ctte e ver nossas deliberações. Mas por favor, não mande
    mensagens relativas a questões específicas para a lista.
</li>

<li>Para postar na lista de discussão do comitê você deve, ou se
    inscrever na lista com o endereço com o qual vai enviar, ou
    assinar sua mensagem com PGP.  Esta é uma medida anti-spam. Nós
    pedimos desculpas pelo inconveniente, mas esta configuração faz com
    que os(as) membros(as) do comitê deem a devida atenção aos e-mails da
    lista do comitê.
</li>

</ul>

<toc-add-entry name="membership">Membros</toc-add-entry>

<p>Os(As) atuais membros(as) do comitê estão listados na página
<a href="$(HOME)/intro/organization#tech-ctte">\
estrutura organizacional do Debian</a>.
</p>

<toc-add-entry name="status">Arquivos e situação atual</toc-add-entry>

<p>A <a href="https://lists.debian.org/debian-ctte/">lista de discussão
do comitê é arquivada</a>.</p>

<p><a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=tech-ctte">
Questões com revisões pendentes</a> podem ser revistas no sistema de
gerenciamento de bugs.</p>

<toc-add-entry name="repository">Repositório VCS</toc-add-entry>

<p>O CT às vezes usa o
<a href="https://salsa.debian.org/debian/tech-ctte">\
repositório git compartilhado</a>
para colaboração.</p>

<h3>Decisões técnicas formais, incluindo recomendações e avisos</h3>

<p> As seções do histórico de decisões não estão necessariamente atualizadas.
  (<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=tech-ctte;archive=yes">Questões
  e decisões mais antigas</a> podem ser vistas no sistema de acompanhamento
  de bugs.)</p>

<ul>
  <li><a href="https://lists.debian.org/debian-devel-announce/2019/03/msg00001.html">2019-03-05</a>
    <a href="https://bugs.debian.org/914897">Bug #914897:</a>
    recusa cancelar a decisão do(a) mantenedor(a)
    de <a href="https://wiki.debian.org/Debootstrap">debootstrap</a>
    sobre habilitar "merged /usr" por padrão na instalação de novos sistemas.
    A decisão também esclareceu sobre a solução desejável do estado de
    "merged /usr" no período de lançamento do Debian Bullseye.</li>
  <li>
    <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00004.html">2018-11-13</a>
    <a href="https://bugs.debian.org/904302">Bug #904302:</a>
    qualquer uso da função de correções em série
    para fornecedores específicos, do pacote dpkg, é um bug para pacotes no
    arquivo do Debian, e tal funcionalidade será proibida no arquivo do Debian
    depois do lançamento do Debian Buster.</li>
  <li>
    <a href="https://lists.debian.org/debian-devel-announce/2018/02/msg00004.html">2018-02-16</a>
    <a href="https://bugs.debian.org/883573">Bug #883573:</a>
    revoga a decisão feita em 2014-11-15 no
    <a href="https://bugs.debian.org/746578">Bug #746578</a> e
    definiu que as dependências do pacote libpam-systemd fiquem livres de
    restrições específicas de ordenamento.</li>
  <li>
    <a href="https://lists.debian.org/debian-devel-announce/2017/07/msg00006.html">2017-07-31</a>
    <a href="https://bugs.debian.org/862051">Bug #862051:</a>
    revoga a decisão feita em 2012-07-12 no
    <a href="https://bugs.debian.org/614907">Bug #614907</a> e
    permite o pacote nodejs prover /usr/bin/node em
    arranjos com retrocompatibilidade.</li>
  <li>2015-09-04
    <a href="https://bugs.debian.org/741573">Bug #741573:</a>
    adota as alterações na política referentes as entradas no
    menu proposto por Charles Plessy, e adicionalmente resolve que
    pacotes que oferecem arquivos de desktop não devem também fornecer um
    arquivo de menu.</li>
<li>2015-06-19
  <a href="https://bugs.debian.org/750135">Bug #750135:</a>
  encoraja Christian Perrier a implementar suas
  propostas para a manutenção do projeto Aptitude.</li>
<li>2014-11-15
  <a href="https://bugs.debian.org/746578">Bug #746578:</a>
  o systemd-shim deve ser a primeira dependência
  alternativa listada do libpam-systemd em vez do systemd-sysv.</li>
<li>2014-08-01
  <a href="https://bugs.debian.org/746715">Bug #746715</a>:
  mantenedores(as) continuem dando suporte aos
  múltiplos sistemas init disponíveis.</li>
<li>2014-08-01
  <a href="https://bugs.debian.org/717076">Bug #717076</a>:
  a implementação da libjpeg padrão deve ser a
  libjpeg-turbo.</li>
<li>2014-02-11
  <a href="https://bugs.debian.org/727708">Bug #727708</a>:
  o sistema init padrão para arquiteturas Linux
  no Jessie deverá ser systemd.</li>
<li>2013-03-06
  <a href="https://bugs.debian.org/698556">Bug #698556</a>:
  anula a exigência feita pelo(a) mantenedor(a) da isdnutils de
  inclusão de código para criar dispositivos isdn por isdnutils.</li>
<li>2012-12-21
  <a href="https://bugs.debian.org/688772">Bug #688772</a>:
  anula a dependência do meta-gnome
  no network-manager enquanto as preocupações levantadas no <a
  href="https://bugs.debian.org/681834#273">§4 da
  decisão em #681834</a> permanecem sem solução.</li>
<li>2012-10-05
  <a href="https://bugs.debian.org/573745">Bug #573745</a>:
  recusa a mudar o(a) mantenedor(a)
  dos pacotes python no Debian.</li>
<li>2012-09-14
  <a href="https://bugs.debian.org/681834">Bug #681834</a>: gnome-core
  deve recomendar: network-manager; sobrepondo a decisão do(a) mantenedor(a).</li>
<li>2012-08-24
  <a href="https://bugs.debian.org/681783">Bug #681783</a>: Política em
  Recommends está correta; Recommends está correto em metapacotes.</li>
<li>2012-08-14
  <a href="https://bugs.debian.org/681687">Bug #681687</a>:
  Falta de entrada do tipo mime para PDF no evince é um bug RC
  (recusa em anular a decisão da equipe de lançamento).</li>
<li>2012-07-12
  <a href="https://bugs.debian.org/614907">Bug #614907</a>:
  nodejs deve usar /usr/bin/nodejs, node deve mudar
  para ax25-node e prover o /usr/sbin/ax25-node, e
  pacotes de transição e legados definidos.</li>
<li>2012-04-05
  <a href="https://bugs.debian.org/640874">Bug #640874</a>: recusa em
  substituir a política de mantenedores(as). debian/rules deve ser um Makefile.</li>
<li>2012-03-21
  <a href="https://bugs.debian.org/629385">Bug #629385</a>:
  dpkg-buildpackage implementará testes de build-arch usando o make-qn.</li>
<li>2012-02-27
  <a href="https://bugs.debian.org/607368">Bug #607368</a>: recusa em
  substituir a política de numeração do ABI do time de mantenedores(as) do
  kernel.</li>
<li>2012-02-05
  <a href="https://bugs.debian.org/658341">Bug #658341</a>: dpkg com multi-arch
  habilitado pode ser enviado para o experimental e instável por Raphaël
  Hertzog sem esperar por uma revisão de código de um(a) mantenedor(a)
  primário(a).</li>
<li>2010-12-01
  <a href="https://bugs.debian.org/587886">Bug #587886</a>:
  lilo deve continuar no instável. Matt Arnold e Joachim
  Wiedorn estão para se juntar aos(às) mantenedores(as) do lilo.</li>
<li>2009-09-04
  <a href="https://bugs.debian.org/535645">Bug #535645</a>:
  recusa em substituir a remoção do ia32-libs-tools pelo time de ftp;
  reafirma a habilidade do time de ftp em remover pacotes;
  recomenda esclarecimento dos motivos para remoção,
  e mecanismo de reintrodução para o repositório.</li>
<li>2009-08-27
  <a href="https://bugs.debian.org/510415">Bug #510415</a>:
  permite o Qmail no Debian depois de corrigir uma questão de delayed-bounce
  com um bug RC bloqueando a transição por um mês</li>
<li>2009-07-30
  <a href="https://bugs.debian.org/539158">Bug #539158</a>:
  recusa em substituir o(a) mantenedor(a) udev; printf sugeriu que seja
  documentado um builtin necessária na política.</li>
<li>2009-07-25
  <a href="https://bugs.debian.org/484841">Bug #484841</a>: por
  padrão, o /usr/local não pode ser escrito pelo grupo staff, mudança pode
  ser implementada depois do plano de transição que permite administradores(as)
  a manter o comportamento atual</li>
<li>2007-12-10
  <a href="https://bugs.debian.org/412976">Bug #412976</a>:
  mantém o comportamento atual e política existente com relação ao uso do
  /etc/default pelo mixmaster.</li>

<li>2007-06-22
  <a href="https://bugs.debian.org/367709">Bug #367709</a>:
  um udeb da libstdc++ não deve ser criado.</li>

<li>2007-06-19
  <a href="https://bugs.debian.org/341839">Bug #341839</a>:
  a saída do <code>md5sum</code> não deve mudar.</li>

<li>2007-04-09
  <a href="https://bugs.debian.org/385665">Bug #385665</a>:
  <code>fluidsynth</code> permanece no main.</li>

<li>2007-04-09
  <a href="https://bugs.debian.org/353277">Bug #353277</a>,
  <a href="https://bugs.debian.org/353278">Bug #353278</a>:
  <code>ndiswrapper</code> permanece no main.</li>

<li>2007-03-27
  <a href="https://bugs.debian.org/413926">Bug #413926</a>:
  <code>wordpress</code> deve ser incluído no etch.</li>

<li>2004-06-24
  <a href="https://bugs.debian.org/254598">Bug #254598</a>:
  <code>amd64</code> é um bom nome para esta arquitetura.
  <a href="https://lists.debian.org/debian-ctte/2004/debian-ctte-200406/msg00115.html">Texto completo</a>.
  A favor: Wichert, Raul, Guy, Manoj, Ian.
  O período de votação foi encerrado mais cedo; nenhum outro voto.</li>
<li>2004-06-05
  <a href="https://bugs.debian.org/164591">Bug #164591</a>,
  <a href="https://bugs.debian.org/164889">Bug #164889</a>:
  <code>md5sum &lt;/dev/null</code> deve produzir o valor md5sum puro (bare).
  <a href="https://lists.debian.org/debian-ctte/2004/debian-ctte-200406/msg00032.html">Texto completo</a>.
  A favor: Guy, Ian, Manoj, Raul.
  Nenhum outro voto.</li>
<li>2002-10-06
  <a href="https://bugs.debian.org/104101">Bug #104101</a>,
  <a href="https://bugs.debian.org/123987">Bug #123987</a>,
  <a href="https://bugs.debian.org/134220">Bug #134220</a>,
  <a href="https://bugs.debian.org/161931">Bug #161931</a>:
  O kernel padrão deve ter suporte a framebuffer VESA incluído.
  <a href="https://lists.debian.org/debian-ctte/2002/debian-ctte-200211/msg00043.html">Texto Completo</a>.
  A favor: Ian, Jason, Raul; contra: Manoj.
  Nenhum outro voto.</li>
<li>2002-07-19 <a href="https://bugs.debian.org/119517">Bug #119517</a>:
  Pacotes podem algumas vezes conter binários cujas bibliotecas
  são somente referenciadas em Suggests.
  <a href="https://lists.debian.org/debian-ctte/2002/debian-ctte-200207/msg00017.html">Texto Completo</a>.
  A favor: Ian, Wichert; contra: Bdale,
  Manoj; ninguém mais votou e Ian usou seu voto de minerva.</li>
</ul>

<p>Note que as decisões tomadas antes de 1 de abril de 2002 ainda
não estão listadas aqui.</p>

<h3>Decisões formais não técnicas e procedurais</h3>

<ul>
<li>2015-03-05 Sam Hartman, Tollef Fog Heen e Didier Raboud aprovados como
    candidatos para o comitê.
    (<a href="https://lists.debian.org/debian-ctte/2015/03/msg00023.html">Texto
    Completo</a>. A favor: Don, Bdale, Andreas, Colin, Steve, Keith.
    Nomeação aprovada pelo DPL 2015-03-08;
    <a href="https://lists.debian.org/debian-devel-announce/2015/03/msg00003.html">Texto
    completo</a>).</li>
<li>2013-11-07 Keith Packard aprovado como membro do comitê técnico (<a href="https://lists.debian.org/debian-ctte/2013/11/msg00041.html">resolução</a>)</li>
<li>2011-08-24 Colin Watson aprovado como membro do comitê técnico (<a href="https://lists.debian.org/debian-devel-announce/2011/08/msg00004.html">nomeação</a>)</li>
<li>2009-01-11 Russ Allbery e Don Armstrong aprovados como membros do comitê técnico (<a href="https://lists.debian.org/debian-ctte/2009/01/msg00053.html">resumo</a>)</li>
<li>2006-04-11 Bdale eleito como presidente (<a href="https://lists.debian.org/debian-ctte/2006/04/msg00042.html">votação</a>)</li>
<li>2006-02-27 Steve eleito como presidente (<a href="https://lists.debian.org/debian-ctte/2006/02/msg00085.html">resumo </a>)</li>
<li>2005-12-20 Steve Langasek, Anthony Towns e Andreas Barth aprovados
    como candidatos para o comitê.
    (<a href="https://lists.debian.org/debian-ctte/2005/12/msg00042.html">Texto
    completo</a>. A favor: Bdale, Manoj. Manifestação de apoio,
    com desculpas, depois do término do período de votação: Ian, Raul.
    Ninguém contra ou abstinente; Nomeação aprovada pelo DPL 2006-01-05;
    <a href="https://lists.debian.org/debian-project/2006/01/msg00013.html">Texto
    completo</a>).</li>
<li>2005-12-20 Proposta de remoção de Wichert, Guy e Jason do comitê.
    (<a href="https://lists.debian.org/debian-ctte/2005/12/msg00000.html">Texto
    da moção</a>; <a href="https://lists.debian.org/debian-ctte/2005/12/msg00028.html">
    resultados</a>. A favor: Manoj, Raul. Guy: a favor de sua própria remoção;
    nenhuma opinião acerca dos demais. Ian: a favor da remoção de Jason;
    contra a remoção dos demais. Remoção aprovada pelo DPL 2006-01-05;
    <a href="https://lists.debian.org/debian-project/2006/01/msg00013.html">Texto
    completo</a>.)</li>
<li>2002-07-05 Questão do uso correto das severidades do sistema de bugs
    (<a href="https://bugs.debian.org/97671">Bug #97671</a>) passada para
    os(às) administradores(as) do BTS e o(a) líder do projeto.
    (<a href="https://lists.debian.org/debian-ctte/2002/debian-ctte-200207/msg00002.html">texto
    completo</a>.  A favor: Ian, Jason, Bdale; ninguém contra ou abstinente).</li>
<li>2002-01-31 Ian Jackson apontado como presidente, após a renúncia de Raul
    do posto. (A favor: Dale, Ian, Manoj, Raul, Wichert;
    ninguém contra ou abstinente).</li>

</ul>

<p>Note que as decisões tomadas antes de 31 de janeiro de 2002 ainda
não estão listadas aqui.</p>

<toc-add-entry name="retiredmembers">Membros que se retiraram</toc-add-entry>

Obrigado às seguintes pessoas que serviram o comitê no passado:

<ul>
<li>Gunnar Wolf (2018-01-02 - 2022-12-31)</li>
<li>Niko Tyni (2017-06-21 - 2022-12-31)</li>
<li>Elana Hashman (2020-05-28 - 2022-07-31)</li>
<li>David Bremner (2017-04-13 - 2021-12-31)</li>
<li>Margarita Manterola (2016-07-07 - 2021-12-31)</li>
<li>Phil Hands (2016-04-16 - 2020-12-31)</li>
<li>Tollef Fog Heen (2015-03-05 - 2019-12-31)</li>
<li>Didier Raboud (2015-03-05 - 2019-12-31)</li>
<li>Keith Packard (2013-11-29 - 2017-12-31)</li>
<li>Sam Hartman (2015-03-08 - 2017-11-09)</li>
<li>Don Armstrong (2009-09-11 - 2016-12-31)</li>
<li>Andreas Barth (2006-01-04 - 2016-12-31)</li>
<li>Steve Langasek (2006-01-04 - 2015-12-31)</li>
<li>Bdale Garbee (2001-04-17 - 2015-12-31)</li>
<li>Colin Watson (2011-08-24 - 2015-03-05)</li>
<li>Ian Jackson (até 2014-11-19)</li>
<li>Russ Allbery (2009-01-11 - 2014-11-16)</li>
<li>Manoj Srivasta (até 2012-08-12)</li>
<li>Anthony Towns (2006-01-04 - 2009-01-05)</li>
<li>Raul Miller (até 2007-04-30)</li>
<li>Wichert Akkerman (até 2006-01-05)</li>
<li>Jason Gunthorpe (até 2006-01-05)</li>
<li>Guy Maor (até 2006-01-05)</li>
<li>Dale Scheetz (até 2002-09-02)</li>
<li>Klee Dienes (até 2001-05-21)</li>

</ul>
