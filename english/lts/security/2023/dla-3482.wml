<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>debian-archive-keyring is a package containing GnuPG archive keys of the Debian
archive. New GPG-keys are being constantly added with every new Debian release.</p>

<p>For Debian 10 buster, GPG-keys for 12/bullseye Debian release are added
in the version 2019.1+deb10u2.</p>

<p>We recommend that you upgrade your debian-archive-keyring packages only if you
need to work with packages from 12/bullseye release.</p>

<p>For the detailed security status of debian-archive-keyring please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/debian-archive-keyring">https://security-tracker.debian.org/tracker/debian-archive-keyring</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3482.data"
# $Id: $
