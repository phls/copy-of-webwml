<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in the interpreter for the Ruby
language. The Common Vulnerabilities and Exposures project identifies the
following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>

    <p>Aaron Patterson reported that WEBrick bundled with Ruby was vulnerable to
    an HTTP response splitting vulnerability. It was possible for an attacker
    to inject fake HTTP responses if a script accepted an external input and
    output it without modifications.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6914">CVE-2018-6914</a>

    <p>ooooooo_q discovered a directory traversal vulnerability in the
    Dir.mktmpdir method in the tmpdir library. It made it possible for
    attackers to create arbitrary directories or files via a .. (dot dot) in
    the prefix argument.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8777">CVE-2018-8777</a>

    <p>Eric Wong reported an out-of-memory DoS vulnerability related to a large
    request in WEBrick bundled with Ruby.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8778">CVE-2018-8778</a>

    <p>aerodudrizzt found a buffer under-read vulnerability in the Ruby
    String#unpack method. If a big number was passed with the specifier @,
    the number was treated as a negative value, and an out-of-buffer read
    occurred. Attackers could read data on heaps if an script accepts an
    external input as the argument of String#unpack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8779">CVE-2018-8779</a>

    <p>ooooooo_q reported that the UNIXServer.open and UNIXSocket.open
    methods of the socket library bundled with Ruby did not check for NUL
    bytes in the path argument. The lack of check made the methods
    vulnerable to unintentional socket creation and unintentional socket
    access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8780">CVE-2018-8780</a>

    <p>ooooooo_q discovered an unintentional directory traversal in
    some methods in Dir, by the lack of checking for NUL bytes in their
    parameter.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.7.358-7.1+deb7u6.</p>

<p>We recommend that you upgrade your ruby1.8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1359.data"
# $Id: $
