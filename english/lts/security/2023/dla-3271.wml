<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A Regular Expression Denial of Service (ReDoS) vulnerability was found
in node-minimatch, a Node.js module used to convert glob expressions
into RegExp objects, which could result in Denial of Service when
calling the <code>braceExpand()</code> function with specific arguments.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.0.4-3+deb10u1.</p>

<p>We recommend that you upgrade your node-minimatch packages.</p>

<p>For the detailed security status of node-minimatch please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-minimatch">https://security-tracker.debian.org/tracker/node-minimatch</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3271.data"
# $Id: $
