<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential heap buffer overflow in
librecad, a popular computer-aided design (CAD) system. A specially crafted
.dxf file could have led to arbitrary code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21897">CVE-2021-21897</a>

    <p>A code execution vulnerability exists in the
    DL_Dxf::handleLWPolylineData functionality of Ribbonsoft dxflib 3.17.0. A
    specially-crafted .dxf file can lead to a heap buffer overflow. An attacker
    can provide a malicious file to trigger this vulnerability.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.1.2-1+deb9u4.</p>

<p>We recommend that you upgrade your librecad packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3046.data"
# $Id: $
