<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes several remote and local denial of service attacks and other 
issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4387">CVE-2013-4387</a>:

<p>ipv6: udp packets following an UFO enqueued packet need
also be handled by UFO to prevent remote attackers to cause a denial of 
service
(memory corruption and system crash) or possibly have unspecified other impact
via network traffic that triggers a large response packet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-4470">CVE-2013-4470</a>:

<p>inet: fix possible memory corruption with UDP_CORK and UFO to
prevent local users to cause a denial of service (memory corruption and system
crash) or possibly gain privileges via a crafted application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0203">CVE-2014-0203</a>:

<p>fix autofs/afs/etc. magic mountpoint breakage, preventing 
denial
of service attacks by local users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2678">CVE-2014-2678</a>:

<p>rds: prevent dereference of a NULL device in rds_iw_laddr_check
to prevent local denial of service attacks (system crash or possibly have 
unspecified other impact).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3122">CVE-2014-3122</a>

<p>: Incorrect locking of memory can result in local denial of
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3144">CVE-2014-3144</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2014-3145">CVE-2014-3145</a>: A local user can cause a denial of service
(system crash) via crafted BPF instructions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3917">CVE-2014-3917</a>:

<p>auditsc: audit_krule mask accesses need bounds checking to
prevent a local denial of service attack (OOPS) or possibly leaking sensitive 
single-bit
values from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4652">CVE-2014-4652</a>:

<p>ALSA: control: Protect user controls against concurrent access,
resulting in a race condition, possibly allowing local users access to 
sensitive
information from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4656">CVE-2014-4656</a>:

<p>ALSA: control: Make sure that id->index does not overflow, to
prevent a denial of service of the sound system by local users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4667">CVE-2014-4667</a>:

<p>sctp: Fix sk_ack_backlog wrap-around problem, preventing denial
of service (socket outage) via a crafted SCTP packet by remote attackers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4699">CVE-2014-4699</a>:

<p>Andy Lutomirski discovered that the ptrace syscall was not
verifying the RIP register to be valid in the ptrace API on x86_64 processors.
An unprivileged user could use this flaw to crash the kernel (resulting in
denial of service) or for privilege escalation.</p></li>

</ul>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in linux-2.6 version 2.6.32-48squeeze8</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0015.data"
# $Id: $
