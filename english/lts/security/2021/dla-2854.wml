<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An XSS vulnerability was discovered in noVNC, a HTML5 VNC client, in
which the remote VNC server could inject arbitrary HTML into the
noVNC web page via the messages propagated to the status field, such
as the VNC server name.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:0.4+dfsg+1+20131010+gitf68af8af3d-6+deb9u1.</p>

<p>We recommend that you upgrade your novnc packages.</p>

<p>For the detailed security status of novnc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/novnc">https://security-tracker.debian.org/tracker/novnc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2854.data"
# $Id: $
