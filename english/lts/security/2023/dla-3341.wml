<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>HTTP multi-header compression denial of service has been fixed in curl,
a command line tool and library for transferring data with URLs.</p>

<p>For Debian 10 buster, this problem has been fixed in version
7.64.0-4+deb10u5.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3341.data"
# $Id: $
