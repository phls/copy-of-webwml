<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in Thunderbird which could
potentially result in the execution of arbitrary code, cross-site
scripting, information disclosure and a covert content attack on S/MIME
encryption using a crafted multipart/alternative message.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:60.9.0-1~deb8u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1926.data"
# $Id: $
