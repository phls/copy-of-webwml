<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Intel® released the INTEL-SA-00766 advisory about potential security
vulnerabilities in some Intel® PROSet/Wireless WiFi and Killer™ WiFi products
may allow escalation of privilege or denial of service. The full advisory is
available at [1]</p>

<p>[1] <a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00766.html">https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00766.html</a></p>

<p>This updated firmware-nonfree package includes the following firmware files:
</br>   - Intel Bluetooth AX2xx series:
</br>      ibt-0041-0041.sfi
</br>      ibt-19-0-0.sfi
</br>      ibt-19-0-1.sfi
</br>      ibt-19-0-4.sfi
</br>      ibt-19-16-4.sfi
</br>      ibt-19-240-1.sfi
</br>      ibt-19-240-4.sfi
</br>      ibt-19-32-0.sfi
</br>      ibt-19-32-1.sfi
</br>      ibt-19-32-4.sfi
</br>      ibt-20-0-3.sfi
</br>      ibt-20-1-3.sfi
</br>      ibt-20-1-4.sfi
</br>    - Intel Wireless 22000 series
</br>      iwlwifi-Qu-b0-hr-b0-77.ucode
</br>      iwlwifi-Qu-b0-jf-b0-77.ucode
</br>      iwlwifi-Qu-c0-hr-b0-77.ucode
</br>      iwlwifi-Qu-c0-jf-b0-77.ucode
</br>      iwlwifi-QuZ-a0-hr-b0-77.ucode
</br>      iwlwifi-cc-a0-77.ucode</p>

<p>The updated firmware files might need updated kernel to work. It is encouraged
to verify whether the kernel loaded the updated firmware file and take
additional measures if needed.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27635">CVE-2022-27635</a>

    <p>Improper access control for some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow a privileged user to potentially enable escalation of
    privilege via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36351">CVE-2022-36351</a>

    <p>Improper input validation in some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow an unauthenticated user to potentially enable denial of
    service via adjacent access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38076">CVE-2022-38076</a>

    <p>Improper input validation in some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow an authenticated user to potentially enable escalation
    of privilege via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40964">CVE-2022-40964</a>

    <p>Improper access control for some Intel(R) PROSet/Wireless WiFi and Killer(TM)
    WiFi software may allow a privileged user to potentially enable escalation of
    privilege via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46329">CVE-2022-46329</a>

    <p>Protection mechanism failure for some Intel(R) PROSet/Wireless WiFi software
    may allow a privileged user to potentially enable escalation of privilege via
    local access.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
20190114+really20220913-0+deb10u2.</p>

<p>We recommend that you upgrade your firmware-nonfree packages.</p>

<p>For the detailed security status of firmware-nonfree please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firmware-nonfree">https://security-tracker.debian.org/tracker/firmware-nonfree</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3596.data"
# $Id: $
