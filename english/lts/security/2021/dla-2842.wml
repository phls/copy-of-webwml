<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Chen Zhaojun of Alibaba Cloud Security Team discovered that JNDI features
used in configuration, log messages, and parameters do not protect
against attacker controlled LDAP and other JNDI related endpoints. An
attacker who can control log messages or log message parameters can
execute arbitrary code loaded from LDAP servers when message lookup
substitution is enabled.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.7-2+deb9u1.</p>

<p>We recommend that you upgrade your apache-log4j2 packages.</p>

<p>For the detailed security status of apache-log4j2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j2">https://security-tracker.debian.org/tracker/apache-log4j2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2842.data"
# $Id: $
