<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libsndfile, a library for reading/writing audio
files.</p>

<p>Using a crafted FLAC file, an attacker could trigger an out-of-bounds read
that would most likely cause a crash but could potentially leak memory
information.</p>


<p>For Debian 10 buster, this problem has been fixed in version
1.0.28-6+deb10u2.</p>

<p>We recommend that you upgrade your libsndfile packages.</p>

<p>For the detailed security status of libsndfile please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libsndfile">https://security-tracker.debian.org/tracker/libsndfile</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3126.data"
# $Id: $
