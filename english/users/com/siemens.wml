# From: r.meier@siemens.com
# Webpage URL is usually redirected, this is normal, Roger Meier 2005-09-20

<define-tag pagetitle>Siemens</define-tag>
<define-tag webpage>https://www.siemens.com/</define-tag>

#use wml::debian::users

<p>
  Siemens is using Debian in several areas including R&amp;D, infrastructure, 
  products, and solutions.
</p>
<p>    
  Debian is used as the basis of many of our products and services in automation 
  and digitalization of process and manufacturing industries, smart mobility 
  solutions for road and rail, digital healthcare services and medical 
  technology as well as distributed energy systems and intelligent 
  infrastructure for buildings.
</p>
<p>
  Debian is for example embedded and part of 
  <a href="https://www.siemens-healthineers.com/magnetic-resonance-imaging">Siemens MRI scanners</a>,
  onboard modern Siemens train and metro systems, around 
  <a href="https://press.siemens.com/global/en/feature/siemens-digitalize-norwegian-railway-network">tracks and train stations</a>,
  and many more of our devices. Furthermore, Siemens is a founding member of the 
  <a href="https://www.cip-project.org/">Civil Infrastructure Platform</a>,
  using Debian LTS as its base layer.
</p>
<p>    
  It goes without saying that Debian is also an essential part of our server 
  infrastructure. Our workstations and desktop machines often run Debian whenever 
  Linux is required, e.g., most Linux based products within the building 
  technology domain are developed on Debian. Siemens is also offering 
  Debian-based embedded distributions, such as the 
  <a href="https://siemens.com/embedded">Sokol™ Omni OS</a>.
</p>
<p>    
  Why we stick with Debian: The high quality and stability we reach by using 
  Debian is just great! and the availability of software updates and security fixes
  is outstanding, and the quantity of available software packages is amazing.
</p>

