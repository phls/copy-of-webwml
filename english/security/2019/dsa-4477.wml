<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Fang-Pen Lin discovered a stack-based buffer-overflow flaw in ZeroMQ, a
lightweight messaging kernel library. A remote, unauthenticated client
connecting to an application using the libzmq library, running with a
socket listening with CURVE encryption/authentication enabled, can take
advantage of this flaw to cause a denial of service or the execution of
arbitrary code.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 4.2.1-4+deb9u2.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 4.3.1-4+deb10u1.</p>

<p>We recommend that you upgrade your zeromq3 packages.</p>

<p>For the detailed security status of zeromq3 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/zeromq3">https://security-tracker.debian.org/tracker/zeromq3</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4477.data"
# $Id: $
