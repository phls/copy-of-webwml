<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Vulnerabilities were found in libssh2, a client-side C library
implementing the SSH2 protocol, which could lead to denial of service or
remote information disclosure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>

    <p>Kevin Backhouse discovered an integer overflow vulnerability in
    <code>kex.c</code>'s <code>kex_method_diffie_hellman_group_exchange_sha256_key_exchange()</code>
    function, which could lead to an out-of-bounds read in the way
    packets are read from the server.  A remote attacker who compromises
    an SSH server may be able to disclose sensitive information or cause
    a denial of service condition on the client system when a user
    connects to the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17498">CVE-2019-17498</a>

    <p>Kevin Backhouse discovered that the <code>SSH_MSG_DISCONNECT</code> logic in
    packet.c has an integer overflow in a bounds check, thereby enabling
    an attacker to specify an arbitrary (out-of-bounds) offset for a
    subsequent memory read.  A malicious SSH server may be able to
    disclose sensitive information or cause a denial of service
    condition on the client system when a user connects to the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22218">CVE-2020-22218</a>

    <p>An issue was discovered in <code>function _libssh2_packet_add()</code>, which
    could allow attackers to access out of bounds memory.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.8.0-2.1+deb10u1.</p>

<p>We recommend that you upgrade your libssh2 packages.</p>

<p>For the detailed security status of libssh2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libssh2">https://security-tracker.debian.org/tracker/libssh2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3559.data"
# $Id: $
