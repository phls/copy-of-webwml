<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential XSS vulnerability in
php-horde-mime-viewer, a MIME viewer library for the Horde groupware
platform.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26874">CVE-2022-26874</a>

    <p>lib/Horde/Mime/Viewer/Ooo.php in Horde Mime_Viewer before 2.2.4 allows
    XSS via an OpenOffice document, leading to account takeover in Horde
    Groupware Webmail Edition. This occurs after XSLT rendering.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
2.2.2-3+deb10u1.</p>

<p>We recommend that you upgrade your php-horde-mime-viewer packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3089.data"
# $Id: $
