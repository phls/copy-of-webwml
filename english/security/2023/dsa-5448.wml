<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2124">CVE-2023-2124</a>

    <p>Kyle Zeng, Akshay Ajayan and Fish Wang discovered that missing
    metadata validation may result in denial of service or potential
    privilege escalation if a corrupted XFS disk image is mounted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2156">CVE-2023-2156</a>

    <p>It was discovered that the IPv6 RPL protocol implementation in the
    Linux kernel did not properly handled user-supplied data, resulting
    in a triggerable assertion. An unauthenticated remote attacker can
    take advantage of this flaw for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2269">CVE-2023-2269</a>

    <p>Zheng Zhang reported that improper handling of locking in the device
    mapper implementation may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3090">CVE-2023-3090</a>

    <p>It was discovered that missing initialization in ipvlan networking
    may lead to an out-of-bounds write vulnerability, resulting in
    denial of service or potentially the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3212">CVE-2023-3212</a>

    <p>Yang Lan that missing validation in the GFS2 filesystem could result
    in denial of service via a NULL pointer dereference when mounting a
    malformed GFS2 filesystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3268">CVE-2023-3268</a>

    <p>It was discovered that an out-of-bounds memory access in relayfs
    could result in denial of service or an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3269">CVE-2023-3269</a>

    <p>Ruihan Li discovered that incorrect lock handling for accessing and
    updating virtual memory areas (VMAs) may result in privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3390">CVE-2023-3390</a>

    <p>A use-after-free flaw in the netfilter subsystem caused by incorrect
    error path handling may result in denial of service or privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31084">CVE-2023-31084</a>

    <p>It was discovered that the DVB Core driver does not properly handle
    locking of certain events, allowing a local user to cause a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32250">CVE-2023-32250</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2023-32254">CVE-2023-32254</a></p>

    <p>Quentin Minster discovered two race conditions in KSMBD, a kernel
    server which implements the SMB3 protocol, which could result in
    denial of service or potentially the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35788">CVE-2023-35788</a>

    <p>Hangyu Hua discovered an out-of-bounds write vulnerability in the
    Flower classifier which may result in denial of service or the
    execution of arbitrary code.</p></li>

</ul>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 6.1.37-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5448.data"
# $Id: $
