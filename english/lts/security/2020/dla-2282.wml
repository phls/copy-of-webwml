<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in Ruby on Rails, a MVC ruby-based
framework geared for web application development, which could lead to
remote code execution and untrusted user input usage, depending on the
application.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8163">CVE-2020-8163</a>

    <p>A code injection vulnerability in Rails would allow an attacker
    who controlled the `locals` argument of a `render` call to perform
    a RCE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8164">CVE-2020-8164</a>

    <p>A deserialization of untrusted data vulnerability exists in rails
    which can allow an attacker to supply information can be
    inadvertently leaked from Strong Parameters.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8165">CVE-2020-8165</a>

   <p>A deserialization of untrusted data vulnernerability exists in
   rails that can allow an attacker to unmarshal user-provided objects
   in MemCacheStore and RedisCacheStore potentially resulting in an
   RCE.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:4.2.7.1-1+deb9u3.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>For the detailed security status of rails please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2282.data"
# $Id: $
