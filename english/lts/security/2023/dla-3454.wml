<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the FFmpeg multimedia
framework, which could result in denial of service or potentially the
execution of arbitrary code if malformed files/streams are processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
7:4.1.11-0+deb10u1.</p>

<p>We recommend that you upgrade your ffmpeg packages.</p>

<p>For the detailed security status of ffmpeg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3454.data"
# $Id: $
