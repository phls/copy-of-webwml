<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in opensc, a set of libraries and
utilities to access smart cards, which could lead to application crash
or information leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6502">CVE-2019-6502</a>

    <p>Dhiraj Mishra discovered a minor memory leak in the <code>eidenv(1)</code> CLI
    utility on an error-case.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42779">CVE-2021-42779</a>

    <p>A heap use after free vulnerability was discovered in
    <code>sc_file_valid()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42780">CVE-2021-42780</a>

    <p>An use after return vulnerability was discovered in <code>insert_pin()</code>,
    which could potentially crash programs using the library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42781">CVE-2021-42781</a>

    <p>Multiple heap buffer overflow vulnerabilities were discovered in
    <code>pkcs15-oberthur.c</code>, which could potentially crash programs using the
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42782">CVE-2021-42782</a>

    <p>Multiple stack buffer overflow vulnerabilities were discovered in
    various places, which could potentially crash programs using the
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2977">CVE-2023-2977</a>

    <p>A buffer overrun vulnerability was discovered in pkcs15
    <code>cardos_have_verifyrc_package()</code>, which could lead to crash or
    information leak via smart card package with a malicious ASN1
    context.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.19.0-1+deb10u2.</p>

<p>We recommend that you upgrade your opensc packages.</p>

<p>For the detailed security status of opensc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/opensc">https://security-tracker.debian.org/tracker/opensc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3463.data"
# $Id: $
