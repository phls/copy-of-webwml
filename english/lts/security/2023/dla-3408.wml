<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in JRuby, a Java implementation of
the Ruby programming language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2019-16254">CVE-2019-16254</a>

    <p>HTTP Response Splitting attacks in the HTTP server of WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16201">CVE-2019-16201</a>

    <p>Regular Expression Denial of Service vulnerability of WEBrick's Digest access authentication.
</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16255">CVE-2019-16255</a>

    <p>Code injection vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25613">CVE-2020-25613</a>

    <p>HTTP Request Smuggling attack in WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31810">CVE-2021-31810</a>

    <p>Trusting FTP PASV responses vulnerability in Net::FTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32066">CVE-2021-32066</a>

    <p>Net::IMAP did not raise an exception when StartTLS fails with an an unknown response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28755">CVE-2023-28755</a>

    <p>Quadratic backtracking on invalid URI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28756">CVE-2023-28756</a>

    <p>The Time parser mishandled invalid strings that have specific characters.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
9.1.17.0-3+deb10u1.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>For the detailed security status of jruby please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jruby">https://security-tracker.debian.org/tracker/jruby</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3408.data"
# $Id: $
