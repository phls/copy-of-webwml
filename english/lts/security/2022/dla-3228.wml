<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>node-json-schema, JSON Schema validation and specifications, was
vulnerable to Improperly Controlled Modification of Object Prototype
Attributes.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.2.3-1+deb10u1.</p>

<p>We recommend that you upgrade your node-json-schema packages.</p>

<p>For the detailed security status of node-json-schema please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-json-schema">https://security-tracker.debian.org/tracker/node-json-schema</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3228.data"
# $Id: $
