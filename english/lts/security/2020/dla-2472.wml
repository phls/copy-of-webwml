<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Mutt, a text-based Mail User Agent, invalid IMAP server responses
were not properly handled, potentially resulting in authentication
credentials being exposed or man-in-the-middle attacks.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.7.2-1+deb9u4.</p>

<p>We recommend that you upgrade your mutt packages.</p>

<p>For the detailed security status of mutt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mutt">https://security-tracker.debian.org/tracker/mutt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2472.data"
# $Id: $
