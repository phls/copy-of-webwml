<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in libuser, a library
that implements a standardized interface for manipulating and
administering user and group accounts, that could lead to a denial of
service or privilege escalation by local users.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3245">CVE-2015-3245</a>

    <p>Incomplete blacklist vulnerability in the chfn function in libuser
    before 0.56.13-8 and 0.60 before 0.60-7, as used in the userhelper
    program in the usermode package, allows local users to cause a
    denial of service (/etc/passwd corruption) via a newline character
    in the GECOS field.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3246">CVE-2015-3246</a>

    <p>libuser before 0.56.13-8 and 0.60 before 0.60-7, as used in the
    userhelper program in the usermode package, directly modifies
    /etc/passwd, which allows local users to cause a denial of service
    (inconsistent file state) by causing an error during the
    modification. NOTE: this issue can be combined with <a href="https://security-tracker.debian.org/tracker/CVE-2015-3245">CVE-2015-3245</a>
    to gain privileges.</p></li>

</ul>

<p>In addition the usermode package, which depends on libuser, was
rebuilt against the updated version.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in</p>

<p>libuser 1:0.56.9.dfsg.1-1.2+deb7u1
usermode 1.109-1+deb7u2</p>

<p>We recommend that you upgrade your libuser and usermode packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-468.data"
# $Id: $
