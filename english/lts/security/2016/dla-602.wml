<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6313">CVE-2016-6313</a>

    <p>Felix Doerre and Vladimir Klebanov from the Karlsruhe Institute of
    Technology discovered a flaw in the mixing functions of GnuPG's
    random number generator. An attacker who obtains 4640 bits from the RNG
    can trivially predict the next 160 bits of output.</p>

    <p>A first analysis on the impact of this bug for GnuPG shows that
    existing RSA keys are not weakened. For DSA and Elgamal keys it is also
    unlikely that the private key can be predicted from other public
    information.</p></li>

<li>Bypassing GnuPG key checking

    <p>Weaknesses have been found in GnuPG signature validation that
    attackers could exploit thanks to especially forged public keys and
    under specific hardware-software conditions. While the underlying
    problem cannot be solved only by software, GnuPG has been
    strengthened, avoiding to rely on keyring signature caches when
    verifying keys. Potential specific attacks are not valid any more
    with the patch of GnuPG</p></li>

<li>Bypassing GnuPG key checking:

    <p>Vrije Universiteit Amsterdam and Katholieke Universteit Leuven
    researchers discovered an attack method, known as Flip Feng Shui,
    that concerns flaws in GnuPG. Researchers found that under specific
    hardware-software conditions, attackers could bypass the GnuPG
    signature validation by using forged public keys. While the
    underlying problem cannot be solved only by software, GnuPG has been
    made more robust to avoid relying on keyring signature caches when
    verifying keys.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these issues have been addressed in version
1.4.12-7+deb7u8.</p>

<p>We recommend that you upgrade your gnupg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-602.data"
# $Id: $
