<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in the '/v2/_catalog' endpoint in
'distribution/distribution', which accepts a parameter to control
the maximum number of records returned (query string: 'n').
This vulnerability allows a malicious user to
submit an unreasonably large value for 'n',
causing the allocation of a massive string array,
possibly causing a denial of service through excessive use of memory.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.6.2~ds1-2+deb10u1.</p>

<p>We recommend that you upgrade your docker-registry packages.</p>

<p>For the detailed security status of docker-registry please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/docker-registry">https://security-tracker.debian.org/tracker/docker-registry</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3473.data"
# $Id: $
