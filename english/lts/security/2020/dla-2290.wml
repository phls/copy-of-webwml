<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in e2fsprogs, a package that contains
ext2/ext3/ext4 file system utilities. A specially crafted ext4 directory
can cause an out-of-bounds write on the stack, resulting in code
execution. An attacker can corrupt a partition to trigger this
vulnerability.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.43.4-2+deb9u2.</p>

<p>We recommend that you upgrade your e2fsprogs packages.</p>

<p>For the detailed security status of e2fsprogs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/e2fsprogs">https://security-tracker.debian.org/tracker/e2fsprogs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2290.data"
# $Id: $
